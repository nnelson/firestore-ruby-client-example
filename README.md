# firestore-ruby-client-example

Firestore ruby client example

## Quick-start

To run this example, clone the project, install the dependencies, and invoke it, as so:

```bash
# Clone:
git clone git@gitlab.com:nnelson/firestore-ruby-client-example.git
cd firestore-ruby-client-example

# Installation:
bundle install --path=vendor/bundle

# Configuration:
# Open 1Password and search for: gitlab-ops-f68d616cbb4f.json
# Open that file in your preferred text editor and save it to a plain text file in the project directory, or copy its contents and:
pbpaste > gitlab-ops-f68d616cbb4f.json

# Execution:
bundle exec ./firestore_client_example.rb
```
