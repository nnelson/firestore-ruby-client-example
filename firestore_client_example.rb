#! /usr/bin/env ruby
# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

# Test for ruby client connection to Firestore.
#
# Installation:
#
# bundle install --path=vendor/bundle
#
# Configuration:
#
# Open 1Password and search for: gitlab-ops-f68d616cbb4f.json
#
# Open that file in your preferred text editor and save it to a plain text file in the project directory:
#
# pbpaste > gitlab-ops-f68d616cbb4f.json
#
# Execution:
#
# bundle exec ./firestore_client_example.rb

require 'google/cloud/firestore'

# FirestoreClientExample module
module FirestoreClientExample
  # Options module
  module Options
    DEFAULT = {
      project_id: 'gitlab-ops',
      credentials: File.join(File.expand_path(__dir__), 'gitlab-ops-f68d616cbb4f.json')
    }
  end

  def initialize_firestore_client(options = ::FirestoreClientExample::Options::DEFAULT)
    firestore = Google::Cloud::Firestore.new(**options)

    puts "Created Cloud Firestore client with project id #{options[:project_id]}"

    firestore
  end

  def get_document(document_id)
    document_reference = firestore_client.doc(document_id)
    document_snapshot = document_reference.get
    document_snapshot
  end

  def add_document(document_id, data)
    document_reference = firestore_client.doc(document_id)
    document_reference.set(data)

    document_snapshot = get_document(document_id)
    puts "Added document #{document_id} in the users collection with data:"
    puts document_snapshot.data.inspect
  end

  def get_all(collection_id)
    data = []
    collection_reference = firestore_client.col(collection_id)
    collection_reference.get do |document|
      data << { document_id: document.document_id, data: document.data }
    end
    data
  end

  def delete_document(document_id)
    document_reference = firestore_client.doc(document_id)
    document_reference.delete
    puts "Deleted document #{document_id}"
  end

  def delete_field(document_id, field)
    document_reference = firestore_client.doc(document_id)
    field_delete_operation = firestore_client.field_delete
    data = {}
    data[field] = field_delete_operation
    document_reference.update(data)
    puts "Deleted field #{field} from the document #{document_id}"
  end

  def delete_collection(collection_id)
    collection_reference = firestore_client.col(collection_id)
    query = collection_reference
    query.get do |document_snapshot|
      puts "Deleting document #{document_snapshot.document_id}"
      document_ref = document_snapshot.ref
      document_ref.delete
    end
    puts "Finished deleting all documents from the collection"
  end
end
# module FirestoreClientExample

# FirestoreClientExample module
module FirestoreClientExample
  class Script
    include FirestoreClientExample
    attr_reader :firestore_client
    def initialize
      @firestore_client = initialize_firestore_client
    end

    def main
      add_document('users/alovelace', first: 'Ada', last: 'Lovelace', born: 1815)
      add_document('users/aturing', first: 'Alan', middle: 'Mathison', last: 'Turing', born: 1912)
      all_users = get_all('users')
      puts "Got all users:"
      puts all_users.inspect

      delete_document('users/alovelace')
      delete_field('users/aturing', 'middle')
      all_users = get_all('users')
      puts "Got all users:"
      puts all_users.inspect

      delete_collection('users')
      all_users = get_all('users')
      puts "Got all users:"
      puts all_users.inspect
    end
    # def main
  end
  # class Script
end
# module FirestoreClientExample

FirestoreClientExample::Script.new.main if $PROGRAM_NAME == __FILE__
